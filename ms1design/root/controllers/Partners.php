<?php namespace Ms1Design\Root\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Partners extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'ms1design.root.projects' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ms1Design.Root', 'root', 'partners');
    }
}