<?php namespace Ms1Design\Root\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;
use Flash;
use Lang;

class Contents extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'ms1design.root.content' ,
        'ms1design.root.translate'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ms1Design.Root', 'root', 'content');
        $this->addCss("/plugins/ms1design/root/assets/css/admin.css", "1.0.0");
    }

    public function onDelete()
    {
    }
}