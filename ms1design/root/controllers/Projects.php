<?php namespace Ms1Design\Root\Controllers;

use Backend\Classes\Controller;
use Ms1Design\Root\Models\Project;
use BackendMenu;
use BackendAuth;
use Lang;
use Flash;

class Projects extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController',
        'Backend\Behaviors\RelationController'
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = [
        'ms1design.root.projects',
        'ms1design.root.translate'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ms1Design.Root', 'root', 'projects');
        $this->addCss("/plugins/ms1design/root/assets/css/admin.css", "1.0.0");
    }

    public function onActivateProjects()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $objectId) {
                if (Project::where('id', $objectId)->where('active', '!=', 1)->count() == 1) {
                    Project::where('id', $objectId)->update(array('active' => 1));
                }
            }

            Flash::success(Lang::get('ms1design.root::lang.projects.flash.activated'));
        }

        return $this->listRefresh();
    }

    public function onDeactivateProjects()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $objectId) {
                if (Project::where('id', $objectId)->where('active', '!=', 0)->count() == 1) {
                    Project::where('id', $objectId)->update(array('active' => 0));
                }
            }

            Flash::success(Lang::get('ms1design.root::lang.projects.flash.deactivated'));
        }

        return $this->listRefresh();
    }

    public function onDelete()
    {
    }

    public function onRemoveProjects()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $objectId) {
                if (Project::where('id', $objectId)->count() == 1) {
                    Project::where('id', $objectId)->delete();
                }
            }

            Flash::success(Lang::get('ms1design.root::lang.projects.flash.deleted'));
        }

        return $this->listRefresh();
    }
}