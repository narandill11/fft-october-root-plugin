<?php namespace Ms1Design\Root\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;
use Flash;
use Lang;
use Ms1Design\Root\Models\Member;

class Members extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\ReorderController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'ms1design.root.members',
        'ms1design.root.translate'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ms1Design.Root', 'root', 'members');
        $this->addCss("/plugins/ms1design/root/assets/css/admin.css", "1.0.0");
    }

    public function listInjectRowClass($record, $definition = null)
    {
        switch ($record->status){
            case 'active':
                return 'row-active';
                break;
            case 'guest':
                return 'frozen';
                break;
            case 'ex-member':
                return 'negative';
                break;
            case 'hidden':
                return 'safe text-muted';
                break;
        }
    }

    public function index()
    {
        $this->vars['MembersStatus'] = Member::MembersStatus();
        $this->vars['LatestMember'] = Member::LatestMember();
        $this->asExtension('ListController')->index();
    }

    public function onActivateMembers()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $objectId) {
                if (Member::where('id', $objectId)->where('active', '!=', 1)->count() == 1) {
                    Member::where('id', $objectId)->update(array('active' => 1));
                }
            }

            Flash::success(Lang::get('ms1design.root::lang.members.flash.activated'));
        }

        return $this->listRefresh();
    }

    public function onDeactivateMembers()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $objectId) {
                if (Member::where('id', $objectId)->where('active', '!=', 0)->count() == 1) {
                    Member::where('id', $objectId)->update(array('active' => 0));
                }
            }

            Flash::success(Lang::get('ms1design.root::lang.members.flash.deactivated'));
        }

        return $this->listRefresh();
    }

    public function onDelete()
    {
    }

    public function onRemoveMembers()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $objectId) {
                if (Member::where('id', $objectId)->count() == 1) {
                    Member::where('id', $objectId)->delete();
                }
            }

            Flash::success(Lang::get('ms1design.root::lang.members.flash.deleted'));
        }

        return $this->listRefresh();
    }
}