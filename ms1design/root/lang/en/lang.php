<?php return [
    'app' => [
        'name' => 'Food Think Tank',
        'tagline' => 'Korzeń',
        'url' => 'www.foodthinktank.pl',
    ],
    'settings' => [
        'locales' => 'Language Management',
        'locales_description' => 'Manage lagnuages available to translate content.',
    ],
    'plugin' => [
        'name' => 'Root',
        'description' => 'Food Think Tank webpage data management plugin.',
    ],
    'global' => [
        'fullname' => 'Name',
        'url' => 'Website URL',
        'id' => 'ID',
        'cover' => 'Cover',
        'slug' => 'Slug',
        'last_edited_by' => 'Updated by',
        'total' => 'Total',
        'created_at' => 'Created at',
        'updated_at' => 'Updated at',
        'not_set' => 'Not set',
        'foodthinktank' => 'Food Think Tank',
        'published_in' => 'Published in',
    ],
    'members' => [
        'profession' => 'Profession',
        'avatar' => 'Avatar',
        'status' => [
            'label' => 'Status',
            'active' => 'Active Member (visible on site)',
            'hidden' => 'Hidden Member (hidden on site)',
            'guest' => 'Guest Member (Participated in projects)',
            'retired' => 'Ex-member (visible on site as ex-member)',
            'active_list' => 'Active',
            'hidden_list' => 'Hidden',
            'guest_list' => 'Guest',
            'retired_list' => 'Ex-member',
            'info' => 'Ex-member',
        ],
        'chart_active' => 'Active Members:',
        'chart_guest' => 'Guest Members:',
        'chart_hidden' => 'Hidden Members:',
        'chart_ex' => 'Ex Members:',
        'chart_total' => 'Total:',
        'chart_last_added' => 'Latest added member',
        'statuscomment' => 'To hide member from entire webstie set status to Hidden. To dim member on website set status to Ex-member.',
        'business' => 'Business name',
        'joined' => 'Since',
        'flash' => [
            'confirmdelete' => 'Are you sure, you want to delete selected members?',
            'activated' => 'Successfully activated selected members',
            'deactivated' => 'Successfully deactivated selected members',
            'deleted' => 'Successfully removed selected members',
        ],
    ],
    'tabs' => [
        'bio' => 'Bio',
        'project' => [
            'description' => 'Description',
            'cover' => 'Cover',
            'gallery' => 'Gallery',
            'participants' => 'Members',
            'partners' => 'Partners',
            'videos' => 'Videos',
            'menu' => 'Menu',
        ],
        'content' => 'Content',
        'attachments' => 'Attachments',
        'footer_image' => 'Footer image',
    ],
    'permissions' => [
        'members' => [
            'access' => 'Members Management Access',
        ],
        'workshop' => [
            'access' => 'Workshop Events Management Access',
        ],
        'projects' => [
            'access' => 'Projects Management Access',
        ],
        'content' => [
            'access' => 'Content Management Access',
        ],
        'news' => [
            'access' => 'News Management Access',
        ],
        'translate' => [
            'access' => 'Page Translation Access (without content edit)',
            'edit' => 'Page Translation Access (with content edit)',
        ],
        'global' => 'Food Think Tank',
    ],
    'menu' => [
        'members' => 'Members',
        'projects' => 'Projects',
        'content' => 'Content',
        'translate' => 'Translate',
        'news' => 'News',
        'workshop' => 'Workshop',
        'subscribers' => 'Newsletter subscribers',
        'partners' => 'Partners & Sponsors',
        'dishes' => 'Dishes',
        'video' => 'Videos',
        'functions' => 'Members functions in projects',
    ],
    'projects' => [
        'title' => 'Title',
        'location' => 'Location',
        'start_date' => 'Started on',
        'end_date' => 'Final',
        'photodocumentation' => 'Add project photos',
        'cover' => 'Project cover photo',
        'addfunction' => 'Add new function to this project and attach members to it.',
        'addmembers' => 'Add new FTT Members to this function.',
        'addvideo' => 'Add Video URL to project',
        'videourl' => 'Paste Youtube full video URL here',
        'videodescription' => 'Video description',
        'memberfunction' => 'Members function in this project',
        'selectmember' => 'Select FTT member',
        'addpartner' => 'Add Partner or Sponsor logotype to this project',
        'partnerurl' => 'Enter Partner or Sponsor Website URL',
        'partnerlogo' => 'Partner or Sponsol logotype',
        'nomembers' => 'No members added',
        'active' => 'Active',
        'activecomment' => 'Switch off to hide project',
        'addtomenu' => 'Add new dish to menu',
        'menu_name' => 'Dish name',
        'menu_description' => 'Dish description',
        'flash' => [
            'confirmdelete' => 'Are you sure, you want to delete selected projects?',
            'activated' => 'Successfully activated selected projects',
            'deactivated' => 'Successfully deactivated selected projects',
            'deleted' => 'Successfully removed selected projects',
        ],
    ],
    'forms' => [
        'required' => [
            'name' => 'Input field Name & Surname is required.',
            'bio' => 'Input field Bio is required.',
            'slug' => 'Input field Slug is required.',
            'avatar' => 'You need to upload member avatar.',
            'title' => 'Input field Project title is required.',
            'location' => 'Input field Location is required.',
            'description' => 'Input field Project description is required.',
            'cover' => 'You need to upload Project cover photo in Project Gallery tab.',
            'page' => 'Please choose page to display this content.',
        ],
    ],
    'news' => [
        'send_disabled' => 'This post is arleady published, automatic send to newsletter list has been turned off, to not harass subscribers. To resend it again – turn switch on.',
        'social_disabled' => 'This post is arleady published, automatic posting to facebook wall has been turned off.',
        'send_to_subscribers' => 'Send e-mail to subscribers',
        'publish_to_facebook' => 'Publish to Facebook fanpage wall',
        'social_published' => 'This post is arleady published on your Facebook fanpage!',
    ],
    'pages' => [
        'project' => [
            'create' => 'Add new project',
            'update' => 'Update project',
            'preview' => 'Preview project',
        ],
        'member' => [
            'create' => 'Add new member',
            'update' => 'Update member',
            'preview' => 'Preview member',
        ],
    ],
    'button' => [
        'delete' => 'Delete',
        'activate' => 'Activate',
        'deactivate' => 'Deactivate',
    ],
    'content' => [
        'select_page' => 'Select page',
        'page' => 'Page',
        'upload' => 'Click here to upload mutliple attachments',
        'no_pages_to_create' => 'All pages contents are arleady created. Please go back to edit content on pages.',
        'add_footer_image' => 'Click to add footer image',
        'add_footer_image_comment' => 'Footer image is optional, if uploaded it will be rendered between page content and footer.',
    ],
    'localizeDate' => [
        'pl_PL' => [
            'january' => 'Styczeń',
            'february' => 'Luty',
            'march' => 'Marzec',
            'april' => 'Kwiecień',
            'may' => 'Maj',
            'june' => 'Czerwiec',
            'july' => 'Lipiec',
            'august' => 'Sierpień',
            'september' => 'Wrzesień',
            'october' => 'Październik',
            'november' => 'Listopad',
            'december' => 'Grudzień',
        ],
        'es_ES' => [
            'january' => 'enero',
            'february' => 'febrero de',
            'march' => 'de arzo de',
            'april' => 'abril',
            'may' => 'Mayo',
            'june' => 'Junio del',
            'july' => 'ulio',
            'august' => 'Agosto',
            'september' => 'Septiembre',
            'october' => 'octubre',
            'november' => 'Noviembre de',
            'december' => 'diciembre',
        ],
    ],
    'partners' => [
        'logotype' => 'Logotype',
        'name' => 'Name',
        'single' => 'Partner or Sponsor',
    ],
    'dishes' => [
        'name' => 'Dish',
        'description' => 'Dish description',
        'served_in' => 'Served on',
    ],
    'videos' => [
        'url' => 'Video',
    ],
    'filters' => [
        'show_active' => 'Show active',
        'show_guest' => 'Show guests',
        'show_ex-member' => 'Show ex-members',
        'show_hidden' => 'Show hidden',
    ],
];