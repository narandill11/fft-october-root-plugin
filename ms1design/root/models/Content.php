<?php namespace Ms1Design\Root\Models;

use Model;
use Flash;
use Lang;
use BackendAuth;
use Ms1Design\Root\Models\Content;
use Cms\Classes\Page;
/**
 * Model
 */
class Content extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    /*
     * Validation
     */
    public $rules = [
        'page'   => 'required',
    ];

    public $translatable = ['content'];

    public $customMessages = [
        'page.required' => 'ms1design.root::lang.forms.required.page',
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ms1design_root_content';

    public $attachMany = [
        'attachments' => 'System\Models\File',
    ];

    public $attachOne = [
        'footer' => 'System\Models\File',
    ];

    public function getPageOptions()
    {
        $pages = [];
        $skipPages = array('post', 'project');

        foreach (Content::all() as $content) {
            if ($this->page != $content->page){
                array_push($skipPages, $content->page);
            }
        }

        foreach (Page::all() as $page) {
            if (!in_array($page['baseFilename'], $skipPages) ){
                $pages[$page['baseFilename']] = '<b>' . $page['title'] . '</b> - ' . $page['description'];
            }
        }

        if (count($pages) <= 0){
            Flash::error(Lang::get('ms1design.root::lang.content.no_pages_to_create'));
            $pages['novalue'] = '<b>' . Lang::get('ms1design.root::lang.content.no_pages_to_create') . '</b>';
        }
        return $pages;
    }

    public function beforeSave()
    {
        $user = BackendAuth::getUser();
        $this->updated_by = $user->first_name . " " . $user->last_name;
        return $this;
    }

    public function beforeCreate()
    {
        if ($this->page == 'novalue') {
            Flash::error(Lang::get('ms1design.root::lang.content.no_pages_to_create'));
            return trigger_error(Lang::get('ms1design.root::lang.content.no_pages_to_create'), E_USER_ERROR);
        }
    }

    public function filterFields($fields, $context = null)
    {
        $user = BackendAuth::getUser();
        $translatable = ['content'];

        if ( !$user->hasPermission(['ms1design.root.content']) && !$user->hasPermission(['ms1design.root.translate_and_edit']) ) {
            foreach ($fields as $field) {
                if ( !in_array($field->fieldName, $translatable) ){
                    if ($field->fieldName === 'name' || $field->fieldName === 'title' || $field->fieldName === 'page' ){
                        $field->disabled = true;
                        $field->span = 'auto';
                        $field->cssClass = '';
                    } else {
                        $field->hidden = true;
                    }
                } else {
                    $field->span = 'auto';
                    $field->cssClass = '';
                }
            }
        }
    }

}