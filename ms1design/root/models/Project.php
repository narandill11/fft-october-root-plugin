<?php namespace Ms1Design\Root\Models;

use Model;
use Ms1Design\Root\Models\Member;
use Ms1Design\Root\Models\MemberFunction;
use BackendAuth;
use Db;
use October\Rain\Exception\ApplicationException;
/**
 * Model
 */
class Project extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
        'name' => 'required',
        'description' => 'required',
        'location' => 'required',
        'cover' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',
    ];

    public $customMessages = [
        'name.required' => 'ms1design.root::lang.forms.required.title',
        'description.required' => 'ms1design.root::lang.forms.required.description',
        'cover.required' => 'ms1design.root::lang.forms.required.cover',
        'start_date.required' => 'ms1design.root::lang.forms.required.start_date',
        'end_date.required' => 'ms1design.root::lang.forms.required.end_date',
        'location.required' => 'ms1design.root::lang.forms.required.location',
    ];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['name', 'description', 'location'];

    protected $slugs = ['slug' => 'name'];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    protected $jsonable = ['members'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ms1design_root_projects';

    public $primaryKey = 'id';

    public $attachOne = [
        'cover' => 'System\Models\File',
    ];

    public $attachMany = [
        'images' => 'System\Models\File',
    ];

    public $belongsToMany = [
        'partner' => [
            'Ms1Design\Root\Models\Partner',
            'table' => 'ms1design_root_project_partners',
        ],
        'dish' => [
            'Ms1Design\Root\Models\Dish',
            'table' => 'ms1design_root_project_dishes',
        ],
        'video' => [
            'Ms1Design\Root\Models\Video',
            'table' => 'ms1design_root_project_videos',
        ],
        'project_members' => [
            'Ms1Design\Root\Models\Member',
            'table' => 'ms1design_root_project_members',
        ],
    ];

    public function beforeDelete()
    {
        // add or remove project-members relations
        $projectMembers = [];
        $allMembers = Member::all();
        $relationTable = 'ms1design_root_project_members';

        foreach ($this->members as $functions) {
            // throw new ApplicationException( json_encode($functions['responsible']) );
            foreach ($functions['responsible'] as $members) {
                array_push($projectMembers, Member::where('name', $members['member'])->first()->id );
                // throw new ApplicationException( json_encode($members['member']) );
            }
        }

        $projectMembers = array_unique($projectMembers);

        foreach ($allMembers as $member) {
            
            // remove relations
            if (in_array($member->id, $projectMembers) == true){
                // throw new ApplicationException( "Deleting relation with user: " . $member->name );
                Db::table($relationTable)
                    ->where('member_id', $member->id)
                    ->where('project_id', $this->id)
                    ->delete();
            }
        }
        return $this;
    }

    public function beforeSave(){
        $user = BackendAuth::getUser();
        $this->last_edited_by = $user->first_name . " " . $user->last_name;
        $this->slug = str_slug($this->name, "-");
    }

    public function afterSave()
    {
        // add or remove project-members relations
        $projectMembers = [];
        $allMembers = Member::all();
        $relationTable = 'ms1design_root_project_members';
        $basename = 'ftt_october';
        
        if (!empty($this->id)){
            $projectID = $this->id;
        } else {
            throw new ApplicationException( "No project ID - plese save again!" );
        }
        foreach ($this->members as $functions) {
            // throw new ApplicationException( json_encode($functions['responsible']) );
            foreach ($functions['responsible'] as $members) {
                array_push($projectMembers, Member::where('name', $members['member'])->first()->id );
            }
        }

        $projectMembers = array_unique($projectMembers);

        foreach ($allMembers as $member) {
            
            // remove relations
            if (in_array($member->id, $projectMembers) == false){
                Db::table($relationTable)
                    ->where('member_id', $member->id)
                    ->where('project_id', $projectID)
                    ->delete();
            // add relations
            } elseif (in_array($member->id, $projectMembers) != false && Db::table($relationTable)->where('member_id', $member->id)->where('project_id', $projectID)->count() < 1 ) {
                $insert = Db::table($relationTable)->insertGetId(
                    ['member_id' => $member->id, 'project_id' => $projectID]
                );
            }
        }
        return $this;
    }

    public function getMemberOptions()
    {
        $members = Member::all();
        $memberOptions = [];

        foreach ($members as $member) {
            if ($member['title'] != ""){
                $title = " (" . $member['title'] . ")";
            } else {
                $title = "";
            }
            
            $memberOptions[$member['name']] = "<b>" . $member['name'] . "</b>" . $title;
        }

        return $memberOptions;
    }

    public function getFunctionOptions()
    {
        $functions = MemberFunction::all();
        $functionsOptions = [];

        foreach ($functions as $function) {
            $functionsOptions[$function['name']] = $function['name'];
        }

        return $functionsOptions;
    }

    public function filterFields($fields, $context = null)
    {
        $user = BackendAuth::getUser();
        $translatable = ['name', 'description', 'location'];

        if ( !$user->hasPermission(['ms1design.root.projects']) && !$user->hasPermission(['ms1design.root.translate_and_edit']) ) {
            foreach ($fields as $field) {
                if ( !in_array($field->fieldName, $translatable) ){
                    if ($field->fieldName === 'name' || $field->fieldName === 'title' || $field->fieldName === 'page' ){
                        $field->disabled = true;
                        $field->span = 'auto';
                        $field->cssClass = '';
                    } else {
                        $field->hidden = true;
                    }
                } else {
                    $field->span = 'auto';
                    $field->cssClass = '';
                }
            }
        }
    }

}