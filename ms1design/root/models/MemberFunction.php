<?php namespace Ms1Design\Root\Models;

use Model;

/**
 * Model
 */
class MemberFunction extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
        'name' => 'required',
    ];
    public $customMessages = [
        'name.required' => 'ms1design.root::lang.forms.required.function_name',
    ];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = ['name'];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ms1design_root_functions';
}