<?php namespace Ms1Design\Root\Models;

use Model;

/**
 * Model
 */
class Partner extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
        'name' => 'required',
        'logotype' => 'required'
    ];

    public $customMessages = [
        'name.required' => 'ms1design.root::lang.forms.required.partner_name',
        'logotype.required' => 'ms1design.root::lang.forms.required.logotype'
    ];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = ['name'];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ms1design_root_partners';

    public $attachOne = [
        'logotype' => 'System\Models\File',
    ];

    public $belongsToMany = [
        'projects' => [
            'Ms1Design\Root\Models\Project',
            'table' => 'ms1design_root_project_partners',
        ],
    ];
}