<?php namespace Ms1Design\Root\Models;

use Model;

/**
 * Model
 */
class Dish extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
        'description' => 'required',
    ];
    public $customMessages = [
        'description.required' => 'ms1design.root::lang.forms.required.dish_description',
    ];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = ['name', 'description'];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ms1design_root_dishes';

    public $belongsToMany = [
        'projects' => [
            'Ms1Design\Root\Models\Project',
            'table' => 'ms1design_root_project_dishes',
        ],
    ];
}