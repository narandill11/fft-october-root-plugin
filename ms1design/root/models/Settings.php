<?php namespace Ms1Design\Root\Models;

use Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'ms1design_root_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}