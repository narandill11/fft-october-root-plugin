<?php namespace Ms1Design\Root\Models;

use Model;

/**
 * Model
 */
class Video extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
        'url' => 'required',
    ];
    public $customMessages = [
        'url.required' => 'ms1design.root::lang.forms.required.url',
    ];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = ['description'];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ms1design_root_videos';

    public $belongsToMany = [
        'projects' => [
            'Ms1Design\Root\Models\Project',
            'table' => 'ms1design_root_project_videos',
        ],
    ];
}