<?php namespace Ms1Design\Root\Models;

use Model;
use BackendAuth;
use DB;

/**
 * Model
 */
class Member extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
        'name' => 'required',
        'bio' => 'required',
    ];

    public $customMessages = [
        'name.required' => 'ms1design.root::lang.forms.required.name',
        'bio.required' => 'ms1design.root::lang.forms.required.bio',
    ];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['title', 'business', 'bio'];

    protected $slugs = ['slug' => 'name'];
    protected $dates = ['created_at', 'updated_at'];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = true;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ms1design_root_members';

    public $primaryKey = 'id';

    public $attachOne = [
        'profile' => 'System\Models\File',
    ];

    public $belongsToMany = [
        'projects' => [
            'Ms1Design\Root\Models\Project',
            'table'    => 'ms1design_root_project_members',
        ],
    ];

    public function scopeIsEnabled($query)
    {
        return $query
            ->whereNotNull('status');
    }

    public function scopeMembersStatus($query)
    {
        if (DB::table('ms1design_root_members')->count() < 1) return 'false';
        return DB::table('ms1design_root_members')->lists('status');
    }

    public function scopeLatestMember($query)
    {
        if (DB::table('ms1design_root_members')->count() < 1) return 'false';
        $latestID = DB::table('ms1design_root_members')->max('id');
        $member = [
            'name' => Member::where('id', $latestID)->first()->name,
            'title' => Member::where('id', $latestID)->first()->title
        ];
        return $member;
    }

    public function beforeCreate()
    {
        if ($this->since == '') {
            $this->since = date('Y-m-d H:i:00');
        }
    }

    public function beforeSave()
    {
        $user = BackendAuth::getUser();
        $this->last_edited_by = $user->first_name . " " . $user->last_name;
        $this->slug = str_slug($this->name, "-");
        return $this;
    }

    public function filterFields($fields, $context = null)
    {
        $user = BackendAuth::getUser();
        $translatable = ['title', 'business', 'bio'];

        if ( !$user->hasPermission(['ms1design.root.members']) && !$user->hasPermission(['ms1design.root.translate_and_edit']) ) {
            foreach ($fields as $field) {
                if ( !in_array($field->fieldName, $translatable) ){
                    if ($field->fieldName === 'name' || $field->fieldName === 'title' || $field->fieldName === 'page' ){
                        $field->disabled = true;
                        $field->span = 'auto';
                        $field->cssClass = '';
                    } else {
                        $field->hidden = true;
                    }
                } else {
                    $field->span = 'auto';
                    $field->cssClass = '';
                }
            }
        }
    }

}