<?php namespace Ms1Design\Root\Components;

use BackendAuth;

class loggedin extends \Cms\Classes\ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'User status',
            'description' => 'Checks if the current user is logged in as admin or editor'
        ];
    }

    public function user(){

        $permissionsStatus = false;
        $allowedPermissions = [
            'ms1design.root.projects',
            'ms1design.root.members',
            'ms1design.root.content',
        ];

        $user = BackendAuth::getUser();

        if (empty($user)){
            return $this->page['user'] = false;
        }

        if (is_array($user->permissions)){
            $userPermissions = implode(",", $user->permissions);
        } else {
            $userPermissions = $user->permissions;
        }

        

        if (strpos($userPermissions, $allowedPermissions[0]) !== false
            || strpos($userPermissions, $allowedPermissions[1]) !== false
            || strpos($userPermissions, $allowedPermissions[2]) !== false)
        {
            $permissionsStatus = true;
        }

        return $this->page['user'] = [
            'superuser'     => $user->is_superuser,
            'permissions'   => $permissionsStatus
        ];
    }
}