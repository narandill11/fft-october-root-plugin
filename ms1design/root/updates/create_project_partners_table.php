<?php namespace Ms1design\Root\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class CreateProjectPartnersTable extends Migration
{
    public function up()
    {
        Schema::create('ms1design_root_project_partners', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('project_id');
            $table->text('partner_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ms1design_root_project_partners');
    }
}
