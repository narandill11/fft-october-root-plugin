<?php namespace Ms1Design\Root\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMs1designRootProjects extends Migration
{
    public function up()
    {
        Schema::table('ms1design_root_projects', function($table)
        {
            $table->boolean('partnership');
        });
    }
    
    public function down()
    {
        Schema::table('ms1design_root_projects', function($table)
        {
            $table->dropColumn('partnership');
        });
    }
}
