<?php namespace Ms1Design\Root\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMs1designRootFunctions extends Migration
{
    public function up()
    {
        Schema::create('ms1design_root_functions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ms1design_root_functions');
    }
}
