<?php namespace Ms1design\Root\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class CreatePartnersTable extends Migration
{
    public function up()
    {
        Schema::create('ms1design_root_partners', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('url');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ms1design_root_partners');
    }
}
