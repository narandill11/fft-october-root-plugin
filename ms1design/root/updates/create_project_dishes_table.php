<?php namespace Ms1design\Root\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class CreateProjectDishesTable extends Migration
{
    public function up()
    {
        Schema::create('ms1design_root_project_dishes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('project_id');
            $table->text('dish_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ms1design_root_project_dishes');
    }
}
