<?php namespace Ms1Design\Root\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMs1designRootProjectMembers extends Migration
{
    public function up()
    {
        Schema::create('ms1design_root_project_members', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('project_id');
            $table->integer('member_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ms1design_root_project_members');
    }
}
