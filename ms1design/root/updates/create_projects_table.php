<?php namespace Ms1design\Root\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class CreateProjectsTable extends Migration
{
    public function up()
    {
        Schema::create('ms1design_root_projects', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('location');
            $table->text('description');
            $table->string('slug');
            $table->text('members');
            $table->string('last_edited_by');
            $table->string('active');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ms1design_root_projects');
    }
}
