<?php namespace Ms1Design\Root\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMs1designRootContent2 extends Migration
{
    public function up()
    {
        Schema::table('ms1design_root_content', function($table)
        {
            $table->dropColumn('attachments');
        });
    }
    
    public function down()
    {
        Schema::table('ms1design_root_content', function($table)
        {
            $table->text('attachments')->nullable();
        });
    }
}
