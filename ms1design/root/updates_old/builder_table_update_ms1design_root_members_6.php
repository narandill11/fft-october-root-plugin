<?php namespace Ms1Design\Root\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMs1designRootMembers6 extends Migration
{
    public function up()
    {
        Schema::table('ms1design_root_members', function($table)
        {
            $table->string('sort_order');
        });
    }
    
    public function down()
    {
        Schema::table('ms1design_root_members', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
