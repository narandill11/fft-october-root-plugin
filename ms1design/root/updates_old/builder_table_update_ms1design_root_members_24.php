<?php namespace Ms1Design\Root\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMs1designRootMembers24 extends Migration
{
    public function up()
    {
        Schema::table('ms1design_root_members', function($table)
        {
            $table->string('active', 10)->nullable(false)->unsigned(false)->default(null)->change();
            $table->dropColumn('retired');
        });
    }
    
    public function down()
    {
        Schema::table('ms1design_root_members', function($table)
        {
            $table->integer('active')->nullable(false)->unsigned(false)->default(null)->change();
            $table->boolean('retired');
        });
    }
}
