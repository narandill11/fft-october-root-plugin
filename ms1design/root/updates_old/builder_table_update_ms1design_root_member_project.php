<?php namespace Ms1Design\Root\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMs1designRootMemberProject extends Migration
{
    public function up()
    {
        Schema::rename('ms1design_root_project_member', 'ms1design_root_member_project');
    }
    
    public function down()
    {
        Schema::rename('ms1design_root_member_project', 'ms1design_root_project_member');
    }
}
