<?php namespace Ms1Design\Root\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMs1designRootMembers13 extends Migration
{
    public function up()
    {
        Schema::table('ms1design_root_members', function($table)
        {
            $table->dropColumn('member_id');
            $table->dropColumn('member_type');
        });
    }
    
    public function down()
    {
        Schema::table('ms1design_root_members', function($table)
        {
            $table->integer('member_id');
            $table->string('member_type', 255);
        });
    }
}
