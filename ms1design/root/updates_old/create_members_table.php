<?php namespace Ms1design\Root\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class CreateMembersTable extends Migration
{
    public function up()
    {
        Schema::create('ms1design_root_members', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('bio')->nullable();
            $table->string('title');
            $table->string('area');
            $table->string('url')->nullable();
            $table->string('slug');
            $table->string('last_edited_by');
            $table->string('status');
            $table->string('business');
            $table->date('since');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ms1design_root_members');
    }
}
