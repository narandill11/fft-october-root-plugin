<?php namespace Ms1Design\Root\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMs1designRootContent extends Migration
{
    public function up()
    {
        Schema::create('ms1design_root_content', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->date('created_at');
            $table->date('updated_at');
            $table->string('updated_by');
            $table->text('content');
            $table->string('page');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ms1design_root_content');
    }
}
