<?php namespace Ms1Design\Root\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMs1designRootMembers7 extends Migration
{
    public function up()
    {
        Schema::table('ms1design_root_members', function($table)
        {
            $table->dropColumn('attachment_id');
            $table->dropColumn('attachment_type');
            $table->dropColumn('field');
            $table->dropColumn('sort_order');
        });
    }
    
    public function down()
    {
        Schema::table('ms1design_root_members', function($table)
        {
            $table->integer('attachment_id');
            $table->string('attachment_type', 255);
            $table->string('field', 255);
            $table->string('sort_order', 255);
        });
    }
}
