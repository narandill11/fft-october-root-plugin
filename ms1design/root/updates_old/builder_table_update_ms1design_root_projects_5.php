<?php namespace Ms1Design\Root\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMs1designRootProjects5 extends Migration
{
    public function up()
    {
        Schema::table('ms1design_root_projects', function($table)
        {
            $table->text('members')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('ms1design_root_projects', function($table)
        {
            $table->string('members', 255)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
