<?php namespace Ms1Design\Root\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMs1designRootProjectMember extends Migration
{
    public function up()
    {
        Schema::table('ms1design_root_project_member', function($table)
        {
            $table->increments('id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('ms1design_root_project_member', function($table)
        {
            $table->dropColumn('id');
        });
    }
}
