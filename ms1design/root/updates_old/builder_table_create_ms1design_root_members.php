<?php namespace Ms1Design\Root\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMs1designRootMembers extends Migration
{
    public function up()
    {
        Schema::create('ms1design_root_members', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('bio')->nullable();
            $table->string('title');
            $table->string('area');
            $table->string('url')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ms1design_root_members');
    }
}
