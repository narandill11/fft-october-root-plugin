<?php namespace Ms1Design\Root\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1014 extends Migration
{
    public function up()
    {
        Schema::table('ms1design_root_projects', function($table)
        {
            
            $table->dropColumn('members');
        });
    }

    public function down()
    {
        $table->json('members');
        // Schema::drop('ms1design_root_table');
    }
}