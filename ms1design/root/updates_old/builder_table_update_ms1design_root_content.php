<?php namespace Ms1Design\Root\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMs1designRootContent extends Migration
{
    public function up()
    {
        Schema::table('ms1design_root_content', function($table)
        {
            $table->text('attachments')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ms1design_root_content', function($table)
        {
            $table->dropColumn('attachments');
        });
    }
}
