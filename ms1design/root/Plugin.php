<?php namespace Ms1Design\Root;

use System\Classes\PluginBase;
use Ms1Design\Root\Models\MemberFunction;
use Lang;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Ms1Design\Root\Components\loggedin' => 'loggedin'
        ];
    }

    public function registerSettings()
    {
        return [
            'facebook' => [
                'label'       => 'Facebook',
                'description' => 'Ustawienia automatycznej publikacji newsów na facebooku',
                'icon'        => 'icon-list-alt',
                'class'       => 'Ms1Design\Root\Models\Settings',
                // 'url'         => Backend::url('ms1design/root/settings'),
                'order'       => 200,
                'category'    => 'ms1design.root::lang.app.name',
                'permissions' => ['ms1design.root::lang.permissions.news.access']
            ]
        ];
    }

    public function registerMarkupTags()
	{
	    return [
	        'filters' => [
	            'orphans' => [$this, 'orphans'],
	        ],
	        'functions' => [
                'video' => [$this, 'videoParser'],
                'slug'  => [$this, 'slugify'],
                'fileSize' => [$this, 'formatSizeUnits'],
                'localizeDate' => [$this, 'localizeDate'],
                'localizeFunction' => [$this, 'localizeFunction'],
	        ]
	    ];
	}

    public function localizeFunction($value, $locale)
    {
        $function = MemberFunction::where('name', $value)->first();
        return $function->lang($locale)->name;
    }

    public function localizeDate($date, $locale)
    {
        if ($locale == 'en_US' || $locale == 'en_EN') return $date;
        $months = [
            'january' => 'January',
            'february' => 'February',
            'march' => 'March',
            'april' => 'April',
            'may' => 'May',
            'june' => 'June',
            'july' => 'July',
            'august' => 'August',
            'september' => 'September',
            'october' => 'October',
            'november' => 'November',
            'december' => 'December',
        ];
        $current = '';

        foreach ($months as $month => $name) {
            if ( strpos(strtolower($date), strtolower($name)) !== false) {
                $current = strtolower($name);
                break;
            }
        }

        $toReplace = [strtoupper($current), ucfirst($current)];
        return str_replace($toReplace, strtolower(Lang::get('ms1design.root::lang.localizeDate.' . $locale . '.' . $current)), $date);
    }

    public function formatSizeUnits($bytes)
        {
            if ($bytes >= 1073741824)
            {
                $bytes = number_format($bytes / 1073741824, 2) . '&nbsp;GB';
            }
            elseif ($bytes >= 1048576)
            {
                $bytes = number_format($bytes / 1048576, 2) . '&nbsp;MB';
            }
            elseif ($bytes >= 1024)
            {
                $bytes = number_format($bytes / 1024, 2) . '&nbsp;KB';
            }
            elseif ($bytes > 1)
            {
                $bytes = $bytes . '&nbsp;B';
            }
            elseif ($bytes == 1)
            {
                $bytes = $bytes . '&nbsp;b';
            }
            else
            {
                $bytes = '0&nbsp;B';
            }

            return $bytes;
    }

    public function slugify( $string )
    {
        return str_slug($string);
    }

    /**
     * get youtube video ID from URL
     *
     * @param string $url
     * @return string Youtube video id or FALSE if none found. 
     */
    public function videoParser($url) {
        $pattern = 
            '%^             # Match any youtube URL
            (?:https?://)?  # Optional scheme. Either http or https
            (?:www\.)?      # Optional www subdomain
            (?:             # Group host alternatives
              youtu\.be/    # Either youtu.be,
            | youtube\.com  # or youtube.com
              (?:           # Group path alternatives
                /embed/     # Either /embed/
              | /v/         # or /v/
              | /watch\?v=  # or /watch\?v=
              )             # End path alternatives.
            )               # End host alternatives.
            ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
            $%x'
            ;
        $result = preg_match($pattern, $url, $matches);
        if ($result) {
            return $matches[1];
        }
        return false;
    }

	public function orphans($content)
	{
	    if ( empty( $content ) ) {
            return;
        }

        /**
         * Keep numbers together - this is independed of current language
         */
        $numbers = true;
        if ( !empty($numbers) ){
            while( preg_match( '/(\d) (\d)/', $content ) ) {
                $content = preg_replace('/(\d) (\d)/', "$1&nbsp;$2", $content );
            }
        }

        $terms = array (
            'al.', 'ale', 'ależ',
            'b.', 'bł.', 'bm.', 'bp', 'br.', 'by', 'bym', 'byś',
            'cyt.', 'cz.', 'czyt.',
            'dn.', 'do', 'doc.', 'dr', 'ds.', 'dyr.', 'dz.',
            'fot.',
            'gdy', 'gdyby', 'gdybym', 'gdybyś', 'gdyż', 'godz.',
            'im.', 'inż.',
            'jw.',
            'kol.', 'komu', 'ks.', 'która', 'którego', 'której', 'któremu', 'który', 'których', 'którym', 'którzy',
            'lic.',
            'max', 'mgr', 'm.in.', 'min', 'moich', 'moje', 'mojego', 'mojej', 'mojemu', 'mój', 'mych', 'na', 'nad', 'np.','nt.', 'nw.',
            'nr', 'nr.', 'nru', 'nrowi', 'nrem', 'nrze', 'nrze', 'nry', 'nrów', 'nrom', 'nrami', 'nrach',
            'od', 'oraz', 'os.',
            'p.', 'pl.', 'pn.', 'po', 'pod', 'pot.', 'prof.', 'przed', 'pt.', 'pw.', 'pw.',
            'śp.', 'św.',
            'tamtej', 'tamto', 'tej', 'tel.', 'tj.', 'to', 'twoich', 'twoje', 'twojego', 'twojej', 'twój', 'twych',
            'ul.',
            'we', 'wg', 'woj.',
            'za', 'ze',
            'że', 'żeby', 'żebyś',
        );

        /**
         * base therms replace
         */
        $re = '/^([aiouwz]|'.preg_replace('/\./', '\.', implode('|', $terms)).') +/i';
        $content = preg_replace( $re, "$1$2&nbsp;", $content );
        /**
         * single letters
         */
        $re = '/([ >\(]+)([aiouwz]|'.preg_replace('/\./', '\.', implode('|', $terms)).') +/i';
        $content = preg_replace( $re, "$1$2&nbsp;", $content );
        /**
         * single letter after previous orphan
         */
        $re = '/(&nbsp;)([aiouwz]) +/i';
        $content = preg_replace( $re, "$1$2&nbsp;", $content );
        /**
         * polish year after number
         */
        $content = preg_replace('/(\d+) (r\.)/', "$1&nbsp;$2", $content);

        /**
         * return
         */
        return $content;
	}
}
